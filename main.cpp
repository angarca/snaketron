#include <SDL.h>
#include "SDL_Rand.hpp"


const Uint32 move = 15,sep = 2;
const Uint32 w = move-sep,h = move-sep;
const Uint32 W =(1200/move)*move,H = (700/move)*move;
const Uint32 Tam = (W*H)/(w*h);
const Uint32 Latencia = 2;
const Uint32 LDibujado = 20;	// * Latencia
const Uint32 LEventos = 5;		// * Latencia
const Uint32 LFisica = 20;		// * Latencia
const Uint32 Xini1 = ((W/4)/move)*move;
const Uint32 Xini2 = ((W/2)/move)*move;
const Uint32 Xini3 = ((W*3/4)/move)*move;
const Uint32 Yini = ((H*3/4)/move)*move;
const Uint32 Uini1 = 20;
const Uint32 Uini2 = 20;
const Uint32 Uini3 = 20;
const Uint32 CpM = 15;
const Uint32 Nbots = 1;
const Uint32 UiniB = 0;
const Uint32 MDB = 10;

enum direccion{
	arriba,
	abajo,
	derecha,
	izquierda
};

inline bool SDL_colision(Uint32 x,Uint32 y, Uint32 w, Uint32 h, Uint32 X, Uint32 Y, Uint32 W, Uint32 H);
inline void SDL_pausa(Uint32 tecla = SDLK_RETURN);

int main(int argc, char *argv[]){

	SDL_Surface *screen;
	SDL_Event evento;
	SDL_Rect dst={0,0,w,h};
	bool salida = false, game_over[3] = {false,false,false};
	bool game_overB[Nbots];
	Uint32 time = SDL_GetTicks();
	Uint32 Tdib = 0x0;
	Uint32 Teve = 0x0;
	Uint32 Tfis = 0x0;
	struct{
		Uint32 x,y;
	}pos1[Tam],pos2[Tam],pos3[Tam],bots[Nbots][Tam];
	Uint32 cab1 = 0x0,usa1 = Uini1;
	Uint32 cab2 = 0x0,usa2 = Uini2;
	Uint32 cab3 = 0x0,usa3 = Uini3;
	Uint32 cabB[Nbots],usaB[Nbots];
	Uint32 direccion1 = SDLK_UP;
	Uint32 direccion2 = SDLK_w;
	Uint32 direccion3 = SDLK_KP_4;
	Uint32 direccion1_aux = SDLK_UP;
	Uint32 direccion2_aux = SDLK_s;
	Uint32 direccion3_aux = SDLK_KP_4;
	direccion botsD[Nbots];
	Uint32 cambiarB[Nbots];
	Uint32 X = (SDL_Rand((W-w*3)/move)+1)*move;
	Uint32 Y = (SDL_Rand((H-h*3)/move)+1)*move;

	for(Uint32 i = 0; i < Nbots; i++){
		cabB[i] = 0x0;
		usaB[i] = UiniB;
		game_over[i] = false;
		botsD[i] = (direccion) (SDL_Rand(9)%4);
		cambiarB[i] = SDL_Rand(MDB)+1;
	}
	
	for(Uint32 i = 0,j = cab1; i < usa1; i++, (++j)%=Tam){
		pos1[j].x = Xini1;
		pos1[j].y = Yini;
	}
	for(Uint32 i = 0,j = cab2; i < usa2; i++, (++j)%=Tam){
		pos2[j].x = Xini2;
		pos2[j].y = Yini;
	}
	for(Uint32 i = 0,j = cab3; i < usa3; i++, (++j)%=Tam){
		pos3[j].x = Xini3;
		pos3[j].y = Yini;
	}
	for(Uint32 i = 0; i < Nbots; i++){
		Uint32 tmpx = (SDL_Rand((W-w*3)/move)+1)*move;
		Uint32 tmpy = (SDL_Rand((H-h*3)/move)+1)*move;
		for(Uint32 j = 0; j < usaB[i]; j++){
			bots[i][j].x = tmpx;
			bots[i][j].y = tmpy;
		}
	}

	SDL_InitSubSystem(SDL_INIT_VIDEO);
//	screen = SDL_SetVideoMode(W,H,32,SDL_HWSURFACE|SDL_DOUBLEBUF);
	SDL_Window *sdlWindow;
	SDL_Renderer *sdlRenderer;
	SDL_CreateWindowAndRenderer(0, 0, SDL_WINDOW_FULLSCREEN_DESKTOP, &sdlWindow, &sdlRenderer);
	SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");  // make the scaled rendering look smoother.
	SDL_RenderSetLogicalSize(sdlRenderer, W,H);
	SDL_SetRenderDrawColor(sdlRenderer, 0, 0, 0, 255);
	SDL_Texture *sdlTexture = SDL_CreateTexture(sdlRenderer,
                               SDL_PIXELFORMAT_ARGB8888,
                               SDL_TEXTUREACCESS_STREAMING,
                               W,H);
	screen = SDL_CreateRGBSurface(0, W,H, 32,
                                        0x00FF0000,
                                        0x0000FF00,
                                        0x000000FF,
                                        0xFF000000);



	do{

		if(Tdib++ >= LDibujado){
			/*if(!game_over[0]){
				dst.x = 0;dst.y = 0;
				SDL_FillRect(screen,&dst,0xff);
			}
			if(!game_over[1]){
				dst.x = 0;dst.y = 0;
				SDL_FillRect(screen,&dst,0xff00);
			}
			if(!game_over[2]){
				dst.x = 0;dst.y = 0;
				SDL_FillRect(screen,&dst,0xffff00);
			}*/
			Tdib = 0x0;
			SDL_FillRect(screen,NULL,0x0);
			for(Uint32 i = 1,j = (cab1+1)%Tam; i < usa1; i++, (++j)%=Tam){
				dst.x = pos1[j].x;dst.y = pos1[j].y;
				SDL_FillRect(screen,&dst,0xff0000);
			}
			for(Uint32 i = 1,j = (cab2+1)%Tam; i < usa2; i++, (++j)%=Tam){
				dst.x = pos2[j].x;dst.y = pos2[j].y;
				SDL_FillRect(screen,&dst,0xff);
			}
			for(Uint32 i = 1,j = (cab3+1)%Tam; i < usa3; i++, (++j)%=Tam){
				dst.x = pos3[j].x;dst.y = pos3[j].y;
				SDL_FillRect(screen,&dst,0xffff00);
			}
			if(usa1){
				dst.x = pos1[cab1].x;dst.y = pos1[cab1].y;
				SDL_FillRect(screen,&dst,0xff7070);
			}if(usa2){
				dst.x = pos2[cab2].x;dst.y = pos2[cab2].y;
				SDL_FillRect(screen,&dst,0x7070ff);
			}if(usa3){
				dst.x = pos3[cab3].x;dst.y = pos3[cab3].y;
				SDL_FillRect(screen,&dst,0xffff70);
			}
			for(Uint32 k = 0; k < Nbots; k++){
				for(Uint32 i = 1,j = (cabB[k]+1)%Tam; i < usaB[k]; i++, (++j)%=Tam){
					dst.x = bots[k][j].x;dst.y = bots[k][j].y;
					SDL_FillRect(screen,&dst,0xffff);
				}
				if(usaB[k]){
					dst.x = bots[k][cabB[k]].x;dst.y = bots[k][cabB[k]].y;
					SDL_FillRect(screen,&dst,0x70ffff);
				}
			}
			dst.x=X;dst.y=Y;
			SDL_FillRect(screen,&dst,0xff00);
//			SDL_Flip(screen);
			SDL_UpdateTexture(sdlTexture, NULL, screen->pixels, screen->pitch);
			SDL_RenderClear(sdlRenderer);
			SDL_RenderCopy(sdlRenderer, sdlTexture, NULL, NULL);
			SDL_RenderPresent(sdlRenderer);

		}

		if(Teve++ >= LEventos){
			Teve = 0x0;
			if(SDL_PollEvent(&evento))
				switch(evento.type){
				case SDL_KEYDOWN:
					if(evento.key.keysym.sym == SDLK_RETURN)
						SDL_pausa();
					else{
						Uint32 e = evento.key.keysym.sym;
						switch(e){
						case SDLK_UP:
							if(direccion1 != SDLK_DOWN)
								direccion1_aux = e;
							break;
						case SDLK_DOWN:
							if(direccion1 != SDLK_UP)
								direccion1_aux = e;
							break;
						case SDLK_RIGHT:
							if(direccion1 != SDLK_LEFT)
								direccion1_aux = e;
							break;
						case SDLK_LEFT:
							if(direccion1 != SDLK_RIGHT)
								direccion1_aux = e;
							break;
						case SDLK_s:
							if(direccion2 != SDLK_z)
								direccion2_aux = e;
							break;
						case SDLK_z:
							if(direccion2 != SDLK_s)
								direccion2_aux = e;
							break;
						case SDLK_a:
							if(direccion2 != SDLK_x)
								direccion2_aux = e;
							break;
						case SDLK_x:
							if(direccion2 != SDLK_a)
								direccion2_aux = e;
							break;
						case SDLK_KP_4:
							if(direccion3 != SDLK_KP_5)
								direccion3_aux = e;
							break;
						case SDLK_KP_5:
							if(direccion3 != SDLK_KP_4)
								direccion3_aux = e;
							break;
						case SDLK_KP_8:
							if(direccion3 != SDLK_KP_2)
								direccion3_aux = e;
							break;
						case SDLK_KP_2:
							if(direccion3 != SDLK_KP_8)
								direccion3_aux = e;
							break;
						case SDLK_ESCAPE:
							salida=true;
							break;
						default:;
						}

					}
					break;
				case SDL_MOUSEBUTTONDOWN:
					game_over[0] = game_over[1] = game_over[2] = false;
					pos1[cab1].x=Xini1;
					pos1[cab1].y=Yini;
					pos2[cab2].x=Xini2;
					pos2[cab2].y=Yini;
					pos3[cab3].x=Xini3;
					pos3[cab3].y=Yini;
					usa1 = Uini1;
					usa2 = Uini2;
					usa3 = Uini3;
					for(Uint32 i = 0,j = cab1; i < usa1; i++, (++j)%=Tam){
						pos1[j].x = Xini1;
						pos1[j].y = Yini;
					}
					for(Uint32 i = 0,j = cab2; i < usa2; i++, (++j)%=Tam){
						pos2[j].x = Xini2;
						pos2[j].y = Yini;
					}
					for(Uint32 i = 0,j = cab3; i < usa3; i++, (++j)%=Tam){
						pos3[j].x = Xini3;
						pos3[j].y = Yini;
					}
					break;
				case SDL_QUIT:
					salida = true;
					break;
				default:;
			}
		}
		
		if(Tfis++ >= LFisica){
			Tfis = 0x0;
			direccion1=direccion1_aux;
			direccion2=direccion2_aux;
			direccion3=direccion3_aux;
			for(Uint32 i = 0; i < Nbots; i++)
				if(!(--cambiarB[i])){
					cambiarB[i] = SDL_Rand(MDB)+1;
					botsD[i] = (direccion) (SDL_Rand(7)%4);
			}
			if(usa1){if(!game_over[0]){if(!cab1)cab1=Tam-1;else cab1--;}else{if(--usa1)game_over[0] = false;}}
			if(usa2){if(!game_over[1]){if(!cab2)cab2=Tam-1;else cab2--;}else{if(--usa2)game_over[1] = false;}}
			if(usa3){if(!game_over[2]){if(!cab3)cab3=Tam-1;else cab3--;}else{if(--usa3)game_over[2] = false;}}
			for(Uint32 i = 0; i < Nbots; i++)
				if(usaB[i]){if(!game_overB[i]){if(!cabB[i])cabB[i]=Tam-1;else cabB[i]--;}else{if(--usaB[i])game_overB[i] = false;}};
			pos1[cab1].x = pos1[(cab1+1)%Tam].x;
			pos1[cab1].y = pos1[(cab1+1)%Tam].y;
			pos2[cab2].x = pos2[(cab2+1)%Tam].x;
			pos2[cab2].y = pos2[(cab2+1)%Tam].y;
			pos3[cab3].x = pos3[(cab3+1)%Tam].x;
			pos3[cab3].y = pos3[(cab3+1)%Tam].y;
			for(Uint32 i = 0; i < Nbots; i++){
				bots[i][cabB[i]].x = bots[i][(cabB[i]+1)%Tam].x;
				bots[i][cabB[i]].y = bots[i][(cabB[i]+1)%Tam].y;
			}
			Uint32 &x1 = pos1[cab1].x;
			Uint32 &y1 = pos1[cab1].y;
			Uint32 &x2 = pos2[cab2].x;
			Uint32 &y2 = pos2[cab2].y;
			Uint32 &x3 = pos3[cab3].x;
			Uint32 &y3 = pos3[cab3].y;
			if(!game_over[0] || usa1)
				switch(direccion1){
				case SDLK_LEFT:
					if(x1 >= move)x1-=move;
					else x1 = ((W-w)/move)*move;
					break;
				case SDLK_RIGHT:
					if(x1+move < W-w)x1+=move;
					else x1 = 0;
					break;
				case SDLK_UP:
					if(y1 >= move)y1-=move;
					else y1 = ((H-h)/move)*move;
					break;
				case SDLK_DOWN:
					if(y1+move < H-h)y1+=move;
					else y1 = 0;
					break;
				default:;
			}/*
			else
			{
				usa1 = Uini1;
				for(Uint32 i = 0,j = cab1; i < usa1; i++, (++j)%=Tam)
				{
						pos1[j].x = Xini1;
						pos1[j].y = Yini;
				}
			}*/
			if(!game_over[1] || usa2)
				switch(direccion2){
				case SDLK_a:
					if(x2 >= move)x2-=move;
					else x2 = ((W-w)/move)*move;
					break;
				case SDLK_x:
					if(x2+move < W-w)x2+=move;
					else x2 = 0;
					break;
				case SDLK_s:
					if(y2 >= move)y2-=move;
					else y2 = ((H-h)/move)*move;
					break;
				case SDLK_z:
					if(y2+move < H-h)y2+=move;
					else y2 = 0;
					break;
				default:;
			}/*
			else
			{
				usa2 = Uini2;
				for(Uint32 i = 0,j = cab2; i < usa2; i++, (++j)%=Tam)
				{
						pos2[j].x = Xini2;
						pos2[j].y = Yini;
				}
			}*/
			if(!game_over[2] || usa3)
				switch(direccion3){
				case SDLK_KP_2:
					if(x3 >= move)x3-=move;
					else x3 = ((W-w)/move)*move;
					break;
				case SDLK_KP_8:
					if(x3+move < W-w)x3+=move;
					else x3 = 0;
					break;
				case SDLK_KP_4:
					if(y3 >= move)y3-=move;
					else y3 = ((H-h)/move)*move;
					break;
				case SDLK_KP_5:
					if(y3+move < H-h)y3+=move;
					else y3 = 0;
					break;
				default:;
			}/*
			else
			{
				usa3 = Uini3;
				for(Uint32 i = 0,j = cab3; i < usa3; i++, (++j)%=Tam)
				{
						pos3[j].x = Xini3;
						pos3[j].y = Yini;
				}
			}*/
			for(Uint32 i = 0; i < Nbots; i++){
				if(!game_overB[i] || usaB[i])
					switch(botsD[i]){
					case izquierda:
						if(bots[i][cabB[i]].x >= move)bots[i][cabB[i]].x-=move;
						else bots[i][cabB[i]].x = ((W-w)/move)*move;
						break;
					case derecha:
						if(bots[i][cabB[i]].x+move < W-w)bots[i][cabB[i]].x+=move;
						else bots[i][cabB[i]].x = 0;
						break;
					case arriba:
						if(bots[i][cabB[i]].y >= move)bots[i][cabB[i]].y-=move;
						else bots[i][cabB[i]].y = ((H-h)/move)*move;
						break;
					case abajo:
						if(bots[i][cabB[i]].y+move < H-h)bots[i][cabB[i]].y+=move;
						else bots[i][cabB[i]].y = 0;
						break;
					default:;
				}
				else
				{
					Uint32 tmpx = (SDL_Rand((W-w*3)/move)+1)*move;
					Uint32 tmpy = (SDL_Rand((H-h*3)/move)+1)*move;
					usaB[i] = UiniB;
					for(Uint32 j = 0,k = cabB[i]; j < usaB[i]; j++, (++k)%=Tam)
					{
							bots[i][k].x = tmpx;
							bots[i][k].y = tmpy;
					}
				}
			}
			if(SDL_colision(x1,y1,w,h,X,Y,w,h) && usa1 < Tam){
				for(Uint32 i = 0,j = (cab1+usa1+1)%Tam; i < CpM; i++, (++j)%=Tam){
					pos1[j].x = pos1[(cab1+usa1)%Tam].x;
					pos1[j].y = pos1[(cab1+usa1)%Tam].y;
				}
				usa1+=CpM;
				X = (SDL_Rand((W-w*3)/move)+1)*move;
				Y = (SDL_Rand((H-h*3)/move)+1)*move;
			}
			if(SDL_colision(x2,y2,w,h,X,Y,w,h) && usa2 < Tam){
				for(Uint32 i = 0,j = (cab2+usa2+1)%Tam; i < CpM; i++, (++j)%=Tam){
					pos2[j].x = pos2[(cab2+usa2)%Tam].x;
					pos2[j].y = pos2[(cab2+usa2)%Tam].y;
				}
				usa2+=CpM;
				X = (SDL_Rand((W-w*3)/move)+1)*move;
				Y = (SDL_Rand((H-h*3)/move)+1)*move;
			}
			if(SDL_colision(x3,y3,w,h,X,Y,w,h) && usa3 < Tam){
				for(Uint32 i = 0,j = (cab3+usa3+1)%Tam; i < CpM; i++, (++j)%=Tam){
					pos3[j].x = pos3[(cab3+usa3)%Tam].x;
					pos3[j].y = pos3[(cab3+usa3)%Tam].y;
				}
				usa3+=CpM;
				X = (SDL_Rand((W-w*3)/move)+1)*move;
				Y = (SDL_Rand((H-h*3)/move)+1)*move;
			}
			for(Uint32 k = 0; k < Nbots; k++)
				if(SDL_colision(bots[k][cabB[k]].x,bots[k][cabB[k]].y,w,h,X,Y,w,h) && usaB[k] < Tam){
					for(Uint32 i = 0,j = (cabB[k]+usaB[k]+1)%Tam; i < CpM; i++, (++j)%=Tam){
						bots[k][j].x = bots[k][(cabB[k]+usaB[k])%Tam].x;
						bots[k][j].y = bots[k][(cabB[k]+usaB[k])%Tam].y;
					}
					usaB[k]+=CpM;
					X = (SDL_Rand((W-w*3)/move)+1)*move;
					Y = (SDL_Rand((H-h*3)/move)+1)*move;
			}
			for(Uint32 i = 0,j = (cab1)%Tam; i < usa1; i++, (++j)%=Tam){
				if(j!=cab1 && SDL_colision(x1,y1,w,h,pos1[j].x,pos1[j].y,h,w))
					game_over[0] = true;
				if(SDL_colision(x2,y2,w,h,pos1[j].x,pos1[j].y,h,w))
					game_over[1] = true;
				if(SDL_colision(x3,y3,w,h,pos1[j].x,pos1[j].y,h,w))
					game_over[2] = true;
				for(Uint32 k = 0; k < Nbots; k++)
					if(SDL_colision(bots[k][cabB[k]].x,bots[k][cabB[k]].y,w,h,pos1[j].x,pos1[j].y,h,w))
						game_overB[k] = true;
			}
			for(Uint32 i = 0,j = (cab2)%Tam; i < usa2; i++, (++j)%=Tam){
				if(SDL_colision(x1,y1,w,h,pos2[j].x,pos2[j].y,h,w))
					game_over[0] = true;
				if(j!=cab2 && SDL_colision(x2,y2,w,h,pos2[j].x,pos2[j].y,h,w))
					game_over[1] = true;
				if(SDL_colision(x3,y3,w,h,pos2[j].x,pos2[j].y,h,w))
					game_over[2] = true;
				for(Uint32 k = 0; k < Nbots; k++)
					if(SDL_colision(bots[k][cabB[k]].x,bots[k][cabB[k]].y,w,h,pos2[j].x,pos2[j].y,h,w))
						game_overB[k] = true;
			}
			for(Uint32 i = 0,j = (cab3)%Tam; i < usa3; i++, (++j)%=Tam){
				if(SDL_colision(x1,y1,w,h,pos3[j].x,pos3[j].y,h,w))
					game_over[0] = true;
				if(SDL_colision(x2,y2,w,h,pos3[j].x,pos3[j].y,h,w))
					game_over[1] = true;
				if(j!=cab3 && SDL_colision(x3,y3,w,h,pos3[j].x,pos3[j].y,h,w))
					game_over[2] = true;
				for(Uint32 k = 0; k < Nbots; k++)
					if(SDL_colision(bots[k][cabB[k]].x,bots[k][cabB[k]].y,w,h,pos3[j].x,pos3[j].y,h,w))
						game_overB[k] = true;
			}
			for(Uint32 l = 0; l < Nbots; l++)
				for(Uint32 i = 0,j = (cabB[l])%Tam; i < usaB[l]; i++, (++j)%=Tam){
					if(SDL_colision(x1,y1,w,h,bots[l][j].x,bots[l][j].y,h,w))
						game_over[0] = true;
					if(SDL_colision(x2,y2,w,h,bots[l][j].x,bots[l][j].y,h,w))
						game_over[1] = true;
					if(SDL_colision(x3,y3,w,h,bots[l][j].x,bots[l][j].y,h,w))
						game_over[2] = true;
					for(Uint32 k = 0; k < Nbots; k++)
						if(j!=cabB[l] && SDL_colision(bots[k][cabB[k]].x,bots[k][cabB[k]].y,w,h,bots[l][j].x,bots[l][j].y,h,w))
							game_overB[k] = true;
			}
		}

		if((time = SDL_GetTicks() - time) < Latencia)
			SDL_Delay(Latencia-time);
		time = SDL_GetTicks();

	}while(!salida);

	return 0;
}

inline bool SDL_colision(Uint32 x,Uint32 y, Uint32 w, Uint32 h, Uint32 X, Uint32 Y, Uint32 W, Uint32 H){
	return (x+w > X && y+h > Y && X+W > x && Y+H > y);
}

inline void SDL_pausa(Uint32 tecla){

	SDL_Event evento;
	while(!SDL_PollEvent(&evento) || evento.type!=SDL_KEYDOWN || evento.key.keysym.sym!=SDLK_RETURN)SDL_Delay(1);

	return;
}
