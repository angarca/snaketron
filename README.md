# README #

This is a simple snake-like game written in SDL, originally was written in 2012 to SDL1.2, but has been ported to SDL2.

To use it, first you need to have installed http://libsdl.org/download-2.0.php under:
    /usr/local/include/SDL2
    /usr/local/lib

Then just use:
    make
    ./main

The controls for the first player (the red one) are the arrow keys, for the second player (the blue one) are left -> 'a', right -> 'x', up -> 's', down -> 'z', and for the third (the yellow one) are the keypad numbers left -> '2', right -> '8', up -> '4', down -> '5'. the return key pause the game, the left button of the mouse reset it, and the escape key quit it.